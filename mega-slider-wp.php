<?php
/**
 * Plugin Name: Mega Slider for wordpress
 * Plugin URI: https://github.com/bioazardmx
 * Description: This plugin adds a Mega Slider for your wordpress theme and is compatible with multilingual addon.
 * Version: 1.0.0
 * Author: Ing Rogelio Jimenez Meza
 * Author URI: https://github.com/bioazardmx
 * License: GPL2
 */
?>
